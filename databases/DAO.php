<?php
namespace Databases;

use Databases\Database;

class DAO extends Database
{
    private $conn;

    public function __construct()
    {
        $dbcon = new parent();
        $this->conn = $dbcon->connect();
    }

    public function select($table, $where = '', $other = '')
    {
        if ($where != '' ) {
            $where = 'where ' . $where;
        }
        $sql = "SELECT * FROM  " .$table. " " . $where . " " . $other;
        $sele = mysqli_query($this->conn, $sql) or die(mysqli_error($this->conn));
        return $sele;
    }

    public function insert($table, $keys = '', $values = '')
    {
        $tableField = "$table ($keys)";
        $sql = "INSERT INTO $tableField
                VALUES ($values)";
        $sele = mysqli_query($this->conn, $sql) or die(mysqli_error($this->conn)); 
        return $sele;       
    }

    public function update($table, $data, $id)
    {
        $sql = "UPDATE $table
                SET $data WHERE id = $id";
        $sele = mysqli_query($this->conn, $sql) or die(mysqli_error($this->conn));
        return $sele;
    }

    public function delete($table, $id)
    {
        $sql = "DELETE FROM $table WHERE id = $id";
        $sele = mysqli_query($this->conn, $sql) or die(mysqli_error($this->conn)); 
        return $sele;
    }
}
