<?php
namespace Databases;

class Database
{
    public function connect()
    {
        $host = '127.0.0.1';
        $user = 'root';
        $pass = '';
        $db = 'todolist';
        $connection = mysqli_connect($host,$user,$pass,$db); 
        return $connection;
    }
}
