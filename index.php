<?php
namespace TodoList;

define('PATH_ROOT', __DIR__);
define('PATH_ROOT_VIEWS', __DIR__ . '/app/views');
define('PATH_ROOT_MODELS', __DIR__ . '/app/models');
define('PATH_ASSETS', (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/public/assets");

use Core\Http\Route;

class TodoList
{
    protected $request_url;

    protected $method_url;

    public function __construct()
    {
        $this->autoloadClass();
        $this->includeFile();

        $this->request_url = !empty($_GET['url']) ? '/' . $_GET['url'] : '/';
        $this->method_url = !empty($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : 'GET';

        $router = new Route();
        // Router
        include_once PATH_ROOT . '/app/routes.php';

        // map URL
        $router->map($this->request_url, $this->method_url);
    }

    private function includeFile()
    {
        // Include helper function
        include_once PATH_ROOT . '/app/common/Helper.php';
    }

    private function autoloadClass()
    {
        // Autoload class trong PHP
        spl_autoload_register(function(string $class_name) {
            $class_name = str_replace('\\', '/', $class_name);
            include_once PATH_ROOT . '/' . $class_name . '.php';
        });
    }
} new TodoList;
