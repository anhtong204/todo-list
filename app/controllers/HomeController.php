<?php

namespace App\Controllers;

use App\Models\Work;

class HomeController extends Controller
{
    protected $work;

    public function __construct()
    {
        $this->work = new Work();
    }

    public function index()
    {
        $allWorks = $this->work->showAllWorks();
        return $this->view('home/index', ['works' => $allWorks]);
    }

    public function calender()
    {
        $allWorks = $this->work->showAllWorks();
        $items = [];
        foreach ($allWorks as $work) {
            $items[] = [
                'id' => $work['id'],
                'title' => $work['name'],
                'calendarId' => 1,
                'category' => 'time',
                'dueDateClass' => '',
                'start' => date("Y-m-d\TH:i:s.000\Z", strtotime($work['start_date'])),
                'end' => date("Y-m-d\TH:i:s.000\Z", strtotime($work['end_date'])),
                'bgColor' => $work['status'] ? 'green' : 'yellow',
                'status' => $work['status']
            ];
        }
        return $this->view('home/calendar', ['works' => $items]);
    }

    public function create()
    {
        $errors = [];
        $allWorks = $this->work->showAllWorks();
        if (isset($_POST['name-work']) && '' == $_POST['name-work']) {
            $errors[] = 'Name Work is required!';
        }

        if (isset($_POST['start-date']) && '' == $_POST['start-date']) {
            $errors[] = 'Starting Date is required!';
        }

        if (isset($_POST['end-date']) && '' == $_POST['end-date']) {
            $errors[] = 'End Date is required!';
        }

        if (!empty($errors)) {
            return $this->view('home/index', ['errors' => $errors, 'works' => $allWorks]);
        }

        $name = addslashes($_POST['name-work']);
        $startDate = addslashes($_POST['start-date']);
        $endDate = addslashes($_POST['end-date']);
        $status = 0;

        $keys = 'name, start_date, end_date, status';
        $values = "'$name', '$startDate', '$endDate', $status"; 

        $result = $this->work->insert($keys, $values);
        if (!$result) {
            $errors[] = 'Error: Insert data has fail!';
        }

        $allWorks = $this->work->showAllWorks();

        return $this->view('home/index', ['messages' => ['New record created successfully'], 'works' => $allWorks]);
    }

    public function updateStatus()
    {
        $response = [
            'result' => false,
            'data' => []
        ];

        if (isset($_POST['workId']) && is_numeric($_POST['workId'])) {
            $wordId = $_POST['workId'];
            $currentWork = $this->work->get($wordId);
            $status = $currentWork['status'];
            $this->work->update("status = !$status", $wordId);
            $response = [
                'result' => true,
                'data' => $this->work->get($wordId)
            ];
        }
        echo json_encode($response);
        die();
    }

    public function delete()
    {
        $response = [
            'result' => false,
            'data' => []
        ];
        $wordId = $_POST['workId'];
        $result = $this->work->delete($wordId);
        if ($result) {
            $response['result'] = true;
        }
        echo json_encode($response);
        die();
    }
}
