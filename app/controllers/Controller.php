<?php
namespace App\Controllers;

class Controller
{
    // public function model($model)
    // {
    //     require_once PATH_ROOT . '/app/models/'. $model .'.php';
    //     return new $model();
    // }

    public function view($view, $data = [])
    {
        require_once PATH_ROOT . '/app/views/'. $view .'.php';
    }
}
