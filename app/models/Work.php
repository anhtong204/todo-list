<?php
namespace App\Models;

use Databases\DAO;

class Work
{
    protected $db;

    protected $table = 'works';

    public function __construct()
    {
        $this->db = new DAO();
    }

    public function showAllWorks()
    {
        $result = $this->db->select($this->table);
        $row = $result->fetch_all(MYSQLI_ASSOC);
        return $row;
    }

    public function get($id)
    {
        $result = $this->db->select($this->table, "id = $id");
        $row = $result->fetch_assoc();
        return $row;
    }

    public function insert($keys, $values)
    {
        if ('' != $keys && '' != $values) {
            $result = $this->db->insert($this->table, $keys, $values);
            return $result;
        }
        return false;
    }

    public function update($data, $id)
    {
        $result = $this->db->update($this->table, $data, $id);
        return $result;
    }

    public function delete($id)
    {
        $result = $this->db->delete($this->table, $id);
        return $result;
    }
}
