<!-- Header -->
<?php get_header(); ?>

<!-- Body -->
<div class="page-content page-container" id="page-content">
    <div class="padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card px-3">
                        <div class="card-body">
                            <h1 class="card-title text-uppercase text-center">Calendar</h1>
                            <a href="/" class="mb-4 d-inline-block">Back to list works</a>
                            <!-- Calendar -->
                            <div id="menu">
                                <span id="menu-navi">
                                    <button type="button" class="btn btn-default btn-sm move-today" data-action="move-today">Today</button>
                                    <button type="button" class="btn btn-default btn-sm move-week" data-action="move-week">Week</button>
                                    <button type="button" class="btn btn-default btn-sm move-month" data-action="move-today">Month</button>
                                    <button type="button" class="btn btn-default btn-sm move-day" data-action="move-prev">
                                        <i class="calendar-icon ic-arrow-line-left" data-action="move-prev"></i>
                                    </button>
                                    <button type="button" class="btn btn-default btn-sm move-day" data-action="move-next">
                                        <i class="calendar-icon ic-arrow-line-right" data-action="move-next"></i>
                                    </button>
                                </span>
                                <span id="renderRange" class="render-range"></span>
                            </div>

                            <div id="calendar"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<?php get_footer(); ?>
<script>
    var cal = new tui.Calendar('#calendar', {
        defaultView: 'month' // daily view option
    });

    $('.move-today').click(function() {
        cal.changeView('day', true);
    })

    $('.move-week').click(function() {
        cal.changeView('week', true);
    })

    $('.move-month').click(function() {
        cal.changeView('month', true);
    })
    
    // Generate data on calendar
    cal.createSchedules(<?php echo json_encode($data['works']); ?>);
</script>
<script src="<?php echo PATH_ASSETS . '/js/calendar/default.js'; ?>"></script>
