<!-- Header -->
<?php get_header(); ?>

<!-- Body -->
<div class="page-content page-container" id="page-content">
    <div class="padding">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card px-3">
                        <div class="card-body">
                            <h1 class="card-title text-uppercase text-center">List Works</h1>

                            <!-- Show Errors -->
                            <?php if (isset($data['errors']) && !empty($data['errors'])): ?>
                                <div class="alert alert-danger" role="alert">
                                    <?php
                                        foreach ($data['errors'] as $error) {
                                            echo $error . '<br/>';
                                        }
                                    ?>
                                </div>
                            <?php endif; ?>

                            <!-- Show Messages -->
                            <?php if (isset($data['messages']) && !empty($data['messages'])): ?>
                                <div class="alert alert-success" role="alert">
                                    <?php
                                        foreach ($data['messages'] as $message) {
                                            echo $message . '<br/>';
                                        }
                                    ?>
                                </div>
                            <?php endif; ?>

                            <form action="/create-work" method="POST">
                                <div class="add-items"> 
                                    <div class="form-group">
                                        <label for="name-work">Work Name</label>
                                        <input type="text" class="form-control" id="name-work" required name="name-work" />
                                    </div>
                                    <div class="form-group">
                                        <label for="name-work">Starting Date</label>
                                        <input type="date" class="form-control" name="start-date" required id="start-date" />
                                    </div>

                                    <div class="form-group">
                                        <label for="name-work">End Date</label>
                                        <input type="date" class="form-control" name="end-date" required id="end-date" />
                                    </div>
                                    <button class="btn btn-primary font-weight-bold">Add Work</button>
                                </div>
                                <em><a href="/calendar">View all works on calendar</a></em>
                            </form>
                            <div class="list-wrapper">
                                <?php if (!empty($data['works'])): ?>
                                    <ul class="d-flex flex-column-reverse todo-list">
                                        <?php foreach ($data['works'] as $work): ?>
                                            <li class="<?php echo isWorkComplete($work['status']); ?>">
                                                <div class="form-check"> 
                                                    <label class="form-check-label" data-id="<?php echo $work['id']; ?>"> 
                                                        <input class="checkbox" type="checkbox" <?php echo $work['status'] ? 'checked' : ''; ?>> 
                                                        <?php printf('%s (%s - %s)', $work['name'], $work['start_date'], $work['end_date']); ?>
                                                        <i class="input-helper"></i>
                                                    </label> 
                                                </div> 
                                                <i class="remove mdi mdi-close-circle-outline" data-id="<?php echo $work['id']; ?>"></i>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                <?php else: ?>
                                    <h3>No works are available.</h3>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Footer -->
<?php get_footer(); ?>