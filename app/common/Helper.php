<?php

if (!function_exists('get_header')) {
    function get_header() {
        include(PATH_ROOT_VIEWS . '/layouts/header.php');
    }
}

if (!function_exists('get_footer')) {
    function get_footer() {
        include(PATH_ROOT_VIEWS . '/layouts/footer.php');
    }
}

/**
 * Check status of work
 */
function isWorkComplete($status = 0) {
    if ($status) {
        return 'completed';
    }
    return '';
}
