<?php

// GET
$router->get('/', 'HomeController@index');
$router->get('/calendar', 'HomeController@calender');

// POST
$router->post('/create-work', 'HomeController@create');
$router->post('/update-status', 'HomeController@updateStatus');
$router->post('/delete', 'HomeController@delete');
