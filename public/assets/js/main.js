jQuery(document).ready(function($) {
    /**
     * Update Status Work
     */
    var updateStatus = function() {
        $('.todo-list').on('click', '.form-check', function(e) {
            e.preventDefault();
            var dis = $(this);
            var id = $(this).find('.form-check-label').attr('data-id');
            $.ajax({
                type: "POST",
                url: '/update-status',
                data: {
                    workId: id
                },
                success: function(response) {
                    response = JSON.parse(response)
                    if (response.result) {
                        if (parseInt(response.data.status)) {
                            dis.parent().addClass('completed');
                            dis.find('input').attr('checked', 'checked');
                        } else {
                            dis.parent().removeClass('completed');
                            dis.find('input').removeAttr('checked');
                        }
                    }
                },
                error: function( jqXHR, textStatus, errorThrown ) {
                    console.log('The following error occured: ' + textStatus, errorThrown)
                }
            })
        });
        return;
    }

    /**
     * Remove Work
     */
    var deleteWork = function() {
        $('i.remove').on('click', function(e) {
            e.preventDefault();
            var id = $(this).data('id');
            var r = confirm("Are you sure delete it?");
            if (r) {
                $.ajax({
                    type: "POST",
                    url: '/delete',
                    data: {
                        workId: id
                    },
                    success: function(response) {
                        response = JSON.parse(response)
                        if (!response.result) {
                            alert('Error when delete work');
                        } else {
                            location.reload();
                        }
                    },
                    error: function( jqXHR, textStatus, errorThrown ) {
                        console.log('The following error occured: ' + textStatus, errorThrown)
                    }
                })
            }
        })
    }

    /**
     * Actions
     */
    updateStatus();
    deleteWork();
});
