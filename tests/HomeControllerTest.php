<?php
namespace Test;

use PHPUnit\Framework\TestCase;
use App\Models\Work;

class HomeControllerTest extends TestCase
{
    public function testFieldsAreFillable()
    {
        $work = new Work();
        $keys = 'name, start_date, end_date, status';
        $name = 'Testing Name';
        $startDate = '2021-02-12';
        $endDate = '2021-02-18';
        $status = 0;
        $values = "'$name', '$startDate', '$endDate', $status"; 
        $result = $work->insert($keys, $values);

        $this->assertEquals($name, $result['name']);
        $this->assertEquals($startDate, $result['start-date']);
        $this->assertEquals($endDate, $result['end-date']);
        $this->assertNotNull($result['status']);
    }
}
