<?php
namespace Core\Http;

class Route
{
    /**
     * Storage array routes
     */
    private $routes;

    public function __construct()
    {
        $this->routes = [];
    }

    /**
     * Method GET
     */
    public function get(string $url, $action)
    {
        return $this->request($url, 'GET', $action);
    }

    /**
     * Method POST
     */
    public function post(string $url, $action)
    {
        return $this->request($url, 'POST', $action);
    }

    /**
     * Handle medthod
     */
    private function request(string $url, string $method, $action)
    {
        // Check URL contains param. Exp: post/{id}
        if (preg_match_all('/({([a-zA-Z]+)})/', $url, $params)) {
            $url = preg_replace('/({([a-zA-Z]+)})/', '(.+)', $url);
        }

        $url = str_replace('/', '\/', $url);

        $route = [
            'url' => $url,
            'method' => $method,
            'action' => $action,
            'params' => $params[2]
        ];
        array_push($this->routes, $route);
    }

    /**
     * Handle URL when calling
     */
    public function map(string $url, string $method)
    {
        foreach ($this->routes as $route) {
            if ($route['method'] == $method) {
                $reg = '/^' . $route['url'] . '$/';
                if (preg_match($reg, $url, $params)) {
                    array_shift($params);
                    $this->call_action_route($route['action'], $params);
                    return;
                }
            }
        }

        echo '404 - Not Found';
        return;
    }

    /**
     * Function call action route
     */
    private function call_action_route($action, $params)
    {
        if (is_callable($action)) {
            call_user_func_array($action, $params);
            return;
        }

        if (is_string($action)) {
            $action = explode('@', $action);
            $controller_name = 'app\\controllers\\' . $action[0];
            $controller = new $controller_name();
            call_user_func_array([$controller, $action[1]], $params);
            return;
        }
    }
}
